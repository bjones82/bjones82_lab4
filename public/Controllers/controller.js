/*global angular*/
var myApp = angular.module('myApp', []);

myApp.controller('AppCtrl', ['$scope', '$http', function($scope, $http) {
    
    var canAdd = true;
    var Gid = "";

    
    var refresh=function() {
        
        
        alert("Gid: " + Gid);        
        $http.get("/commentlist").success(function(response){
            var selectivelist = [];
            for (var i=0; i<response.length; i++){
                if(response[i].googleid == Gid)
                    selectivelist.push(response[i]);
            }
            $scope.commentlist = selectivelist;
            $scope.Comment = "";
        });
    };
    
    refresh();
    
    $scope.addComment = function(){
        if(canAdd){
            alert("Gid: " + Gid);
            $scope.Comment.googleid = Gid;
            $http.post("/commentlist", $scope.Comment).success(function(response){
                refresh();   
        });
    }};
    
    $scope.removeComment = function(id){
        $http.delete("/commentlist/" + id).success(function(response){
            refresh();
        });
    };
    
    $scope.editComment = function(id){
      $http.get("/commentlist/" + id).success(function(response){
          $scope.Comment = response;
          canAdd = false;
      }); 
    };
    
    $scope.updateComment = function(){
      $http.put("/commentlist/" + $scope.Comment._id, $scope.Comment).success(function(response){
          refresh();
          canAdd = true;
      });
    };
    
    $scope.deselect = function(){
        $scope.Comment = "";
        canAdd = true;
    };
    
    $scope.auth = function() {
        var config = {
          'client_id': '538280501413-siupeodg0a6tnnqsb4vn7gao3jirrkj5.apps.googleusercontent.com',
          'scope': 'https://www.googleapis.com/auth/userinfo.profile'
        };
        gapi.auth.authorize(config, function() {
            var urlToken = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + gapi.auth.getToken().access_token;
            $.getJSON(urlToken, function (json) {
                Gid = json.id;
                refresh();
            });
        });
        refresh();
      };
      

    
    
}]);    