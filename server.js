var express = require('express');
var mongojs = require('mongojs');
var bodyParser = require("body-parser");
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var app = express();
var db = mongojs('CommentData', ['CommentData']);
var userdb = mongojs('UserData', ['UserData']);
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var session = require("express-session");
var configDB = require('./config/database.js');


require("./app/routes.js");

app.use(morgan("dev"));
app.use(cookieParser());
app.use(session({secret: 'secretstring',
    saveUninitialized: true,
    resave: true
}));


app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.get("/commentlist", function(req, res) {
    db.CommentData.find(function(err, docs) {
        res.json(docs);
    });
});

app.post("/commentlist", function(req, res) {
    db.CommentData.insert(req.body, function(err, doc) {
        res.json(doc);
    });
});

app.delete("/commentlist/:id", function(req, res) {
    var id = req.params.id;
    db.CommentData.remove({
        _id: mongojs.ObjectId(id)
    }, function(err, doc) {
        res.json(doc);
    });
});

app.get("/commentlist/:id", function(req, res) {
    var id = req.params.id;
    db.CommentData.findOne({
        _id: mongojs.ObjectId(id)
    }, function(err, doc) {
        res.json(doc);
    });
});

app.put("/commentlist/:id", function(req, res) {
    var id = req.params.id;
    db.CommentData.findAndModify({
        query: {
            _id: mongojs.ObjectId(id)
        },
        update: {
            $set: {
                name: req.body.name,
                email: req.body.email,
                message: req.body.message,
                time: req.body.time
            }
        },
        new: true
    }, function(err, doc) {
        res.json(doc);
    });
});

app.listen(8080);
