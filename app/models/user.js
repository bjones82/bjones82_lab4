var mongojs = require('mongojs');
var mongoose = require('mongoose');
var userdb = mongojs('UserData', ['UserData']);

var userScheme = mongoose.Schema({
    local: {
        username: String,
        password: String
    }
});

module.exports = mongoose.model('User',userScheme);